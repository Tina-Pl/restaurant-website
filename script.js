window.addEventListener('scroll', function () {
  changeNavColor();
  toggleToTopBtn();
  changeActiveLink();
});

function addClass(element, className) {
  if (Array.isArray(element)) {
    element.forEach(el => {
      el.classList.add(className);
    });
  } else {
    element.classList.add(className);
  }
}

function removeClass(element, className) {
  if (Array.isArray(element)) {
    element.forEach(el => {
      el.classList.remove(className);
    });
  } else {
    element.classList.remove(className);
  }
}

function changeNavColor() {
  const elements = {
    nav: document.querySelector('.navbar.navbar-inverse.my-navbar'),
    collapseNavbar: document.querySelector('.navbar-inverse .my-nav'),
    navbarToggle: document.querySelector('.navbar-toggle'),
  };

  const navigation = Object.values(elements);

  window.scrollY > 700
    ? addClass(navigation, 'white')
    : removeClass(navigation, 'white');
}

const scrollLinks = document.querySelectorAll('.scroll');

function changeActiveLink() {
  const scrollBarLocation = window.scrollY;
  const pageScrollHeight = document.body.scrollHeight;
  const scrollPoint = scrollBarLocation + window.innerHeight;

  for (let link of scrollLinks) {
    const hash = link.hash;
    const sectionOffset = document.querySelector(hash).offsetTop - 30;

    if (sectionOffset <= scrollBarLocation || scrollPoint >= pageScrollHeight) {
      if (!link.parentElement.classList.contains('active')) {
        addClass(link.parentElement, 'active');

        const linkSiblings = getSiblings(link);

        for (let linkSibling of linkSiblings) {
          removeClass(linkSibling, 'active');
        }
      }
    }
  }
}

function getSiblings(el) {
  const parent = document.querySelector('.nav');
  let sibling = parent.firstChild.nextSibling;
  let siblings = [];

  while (sibling) {
    if (sibling.nodeType === 1 && sibling !== el.parentElement) {
      siblings.push(sibling);
    }
    sibling = sibling.nextSibling;
  }
  return siblings;
}

scrollLinks.forEach(link => {
  link.addEventListener('click', handleSmoothScroll);
});

function handleSmoothScroll(event) {
  event.preventDefault();

  const hash = this.hash;
  const offsetTop = document.querySelector(hash).offsetTop;

  scroll({
    top: offsetTop,
    behavior: 'smooth',
  });

  setTimeout(() => {
    window.location.hash = hash;
  }, 800);
}

const upBtn = document.querySelector('#upBtn');

upBtn.addEventListener('click', scrollToTop);

function toggleToTopBtn() {
  window.scrollY > 750 ? addClass(upBtn, 'show') : removeClass(upBtn, 'show');
}

function scrollToTop() {
  window.scroll({
    top: 0,
    behavior: 'smooth',
  });
}

function getMenu() {
  fetch('menu.json')
    .then(response => response.json())
    .then(data => {
      console.log(data.menu);
      displayMenu(data.menu);
    })
    .catch(error => console.log(`'Error: ${error}`));
}

getMenu();

function displayMenu(data) {
  const filterBtn = document.querySelectorAll('.filter-btn');

  filterBtn.forEach(btn => {
    btn.addEventListener('click', function (event) {
      event.preventDefault();

      const currentBtn = document.getElementsByClassName('active-btn');
      currentBtn[0].className = currentBtn[0].className.replace(
        'active-btn',
        ''
      );
      this.className += ' active-btn';

      const btnValue = event.target.dataset.filter;

      const dailyMenu = data.filter(el => el.day.toLowerCase() === btnValue);

      populateMenu(dailyMenu[0]);
    });
  });
}

function populateMenu(data) {
  const soupName = document.querySelector('.soup');
  const soupPrice = document.querySelector('.price1');
  const mainDish = document.querySelector('.main');
  const mainDishPrice = document.querySelector('.price2');
  const dessertName = document.querySelector('.dessert');
  const dessertPrice = document.querySelector('.price3');

  soupName.textContent = data.soup;
  soupPrice.innerHTML = data.soupPrice + ' &euro;';
  mainDish.textContent = data.main;
  mainDishPrice.innerHTML = data.mainPrice + ' &euro;';
  dessertName.textContent = data.dessert;
  dessertPrice.innerHTML = data.dessertPrice + ' &euro;';
}
