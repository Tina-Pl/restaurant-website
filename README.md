# Restaurant Website

---

This single page Restaurant website is a personal project. At first, the basic idea behind this project was to come up with a design, make a hand-drawn wireframe and build the website using HTML5, CSS3 and Bootstrap.  
Later, I decided to add something more to this website and use it to practice JavaScript. I used it to make sticky nav, smooth scrolling, add scroll to top button and change active link on scroll. In the Daily Menu section I used it to add active class to current control button and based on that display/change text content of daily menu elements.

## Built With

---

- HTML5
- CSS3
- Bootstrap
- JavaScript
- [Ionicons](https://ionicons.com/v2) - Icons
- [Pixabay](https://pixabay.com/) - Photos
